package si.ijs.e6;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by matjaz on 9/14/16.
 */
public class S2DataGetter implements S2.ReadLineCallbackInterface {
    class StreamingDataPacket {
        long timestamp;
        byte data[];

        StreamingDataPacket(long t, byte d[]) {
            timestamp = t;
            data = d;
        }
    }

    S2.LoadStatus inputLoadStatus;
    List<StreamingDataPacket> data = new ArrayList<StreamingDataPacket>();
    byte streamHandle;
    boolean endOfFileReached = false;
    S2.DataEntityCache streamingSensorMetadata;
    // holds the last decoded index to speed up execution when the ecg decoding is executed in a loop
    long lastDecodedEcgCounter = 0;

    // TODO: setting data getter to a particular handle is not the most elegant but is effective enough for now
    S2DataGetter(S2.LoadStatus inputLoadStatus, byte handle) {
        this.inputLoadStatus = inputLoadStatus;
        streamHandle = handle;
    }

    public boolean onComment(String comment) { return true; }
    public boolean onVersion(int versionInt, String extendedVersion) { return true; }
    public boolean onSpecialMessage(char who, char what, String message) { return true; }
    public boolean onMetadata(String key, String value) { return true; }
    public boolean onDefinition(byte handle, S2.SensorDefinition definition) { return true; }
    public boolean onDefinition(byte handle, S2.StructDefinition definition) { return true; }
    public boolean onDefinition(byte handle, S2.TimestampDefinition definition) { return true; }
    public boolean onTimestamp(long nanoSecondTimestamp) { return true; }
    public boolean onUnknownLineType(byte type, int len, byte data[]) { return true; }
    public boolean onError(int lineNum,  String error) { return true; }

    public boolean onEndOfFile() {
        endOfFileReached = true;
        return true;
    }

    @Override
    public boolean onUnmarkedEndOfFile() {
        return true;
    }

    public boolean onStreamPacket(byte handle, long timestamp, int len, byte data[]) {
        if (handle == streamHandle)
            this.data.add(new StreamingDataPacket(timestamp, data));
        return true;
    }

    // TODO: make this function general (now ECG type 0b00 packets are hardcoded in)
    public boolean decodePacket(StreamingDataPacket packet) {
        // link to handle definition
        if (streamingSensorMetadata == null) {
            streamingSensorMetadata = inputLoadStatus.getDataEntity(streamHandle);
            if (streamingSensorMetadata == null)
                throw new RuntimeException("Cann ot decode a packet, handle "+streamHandle+" is missing definition");
        }

        double seconds = packet.timestamp/1000000000.0;
        ArrayList<Float> sensorData = new ArrayList<>();
        MultiBitBuffer mbb = new MultiBitBuffer(packet.data);	// Getting 10-bit sample from 19-Bytes
        for (int i = 0; i < 14; ++i) {
            int temp = mbb.getInt(i*10, 10);
            sensorData.add(temp*1.0f);
        }
        int tenBitTime = mbb.getInt(14*10, 10); // 10-bit sample counter

        return true;
    }

    public boolean decodeECG(StreamingDataPacket packet) {
        double seconds = packet.timestamp/1000000000.0;
        ArrayList<Short> sensorData = new ArrayList<>();
        MultiBitBuffer mbb = new MultiBitBuffer(packet.data);	// Getting 10-bit sample from 19-Bytes
        for (int i = 0; i < 14; ++i) {
            short temp = mbb.getShort(i*10, 10);
            sensorData.add(temp);
        }
        short tenBitTime = mbb.getShort(14*10, 10); // 10-bit sample counter

        return true;
    }

    /**
     * Decode and copy ECG samples from the internal storage (must be read from s2 first) to the supplied destination array list.
     * Decoding and copying will stop after either #maxElemets have been copied or a missing sample has been found
     *
     * @param destinationStartTime    the start time of the copied samples
     * @param destination    the destination buffer for ECG samples
     * @param from           starting index to start copying from
     * @param maxSamples     maximum number of samples to copy (may copy less)
     * @return the number of samples copied
     */
    public int decodeECG(Long destinationStartTime, ArrayList<Float> destination, long from, long maxSamples) {
        int packetIndex = (int)(from / 14);

        StreamingDataPacket lastPacket = data.get(packetIndex);
        // round down maxSamples to the nearest multiple of 14
        maxSamples = (maxSamples / 14) * 14;

        long counter = lastDecodedEcgCounter;
        int i = 0;
        for (; packetIndex < Math.min(maxSamples, data.size());) {
            StreamingDataPacket packet = data.get(packetIndex);
            MultiBitBuffer mbb = new MultiBitBuffer(packet.data);	// Getting 10-bit sample from 19-Bytes
            for (int j = 0; j < 14; ++j) {
                short temp = mbb.getShort(j*10, 10);
                destination.add(temp != 0 ? 0.00594367f*temp : Float.NaN);
                //destination.add(temp*1.0f);
            }
            i += 14;
            short tenBitTime = mbb.getShort(14*10, 10); // 10-bit sample counter
            short prevTenBitTime = (short)(counter & 0x03FF);
            while (tenBitTime < prevTenBitTime) {
                counter += 1024;
            }

            ++packetIndex;
        }
        lastDecodedEcgCounter = counter;
        return i;
    }
}
