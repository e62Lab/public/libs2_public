package si.ijs.e6;

import java.util.ArrayList;

import si.ijs.e6.S2.AbsoluteId;
import si.ijs.e6.S2.DeviceType;
import si.ijs.e6.S2.DataEntityCache;
import si.ijs.e6.S2.MessageType;
import si.ijs.e6.S2.ReadLineCallbackInterface;
import si.ijs.e6.S2.SensorDefinition;
import si.ijs.e6.S2.StructDefinition;
import si.ijs.e6.S2.TimestampDefinition;
import si.ijs.e6.S2.ValueType;

public class CallbackConsoleOutput implements ReadLineCallbackInterface{

	//private long timestamp = 0;
	S2 s2;
	
	public CallbackConsoleOutput(S2 file)
	{
		s2 = file;
	}
	
	public boolean onComment(String comment)
	{
		long maxTimestamp = 0;
		for (DataEntityCache c : s2.getCachedHandles()) {
			if ((c != null) && (c.lastAbsTimestamp.getValue() > maxTimestamp))
				maxTimestamp = c.lastAbsTimestamp.getValue();
		}
		
		double toSeconds = maxTimestamp / 1000000000.0;
		String seconds = String.valueOf(Math.round(toSeconds * 100.0) / 100.0);	// Rounds to two decimal places
		System.out.println(String.format("%12s %15s : %s" , seconds + "s", "comment", comment ));
		return true;
	}

	public boolean onVersion(int versionInt, String extendedVersion)
    {
		System.out.println(String.format("%28s : %s %s" , "version", S2.getStringVersion(versionInt), extendedVersion));
    	return true;
    }

	public boolean onSpecialMessage(char who, char what, String message)
	{
		long maxTimestamp = 0;
		
		for(DataEntityCache c : s2.getCachedHandles()) {
			if ((c != null) && (c.lastAbsTimestamp != null)) {
				if (c.lastAbsTimestamp.getValue() > maxTimestamp)
					maxTimestamp = c.lastAbsTimestamp.getValue();
			}
		}
		
		double toSeconds = maxTimestamp / 1000000000.0;
		
    	//double toSeconds = s2.getCachedHandles()/1000000000.0;	// Converts nanoseconds into seconds
    	String seconds = String.valueOf(Math.round(toSeconds * 100.0) / 100.0);	// Rounds to two decimal places

        DeviceType dt = DeviceType.convert((byte)who);
        MessageType mt = MessageType.convert((byte)what);

		System.out.println(String.format("%12s %15s : %s: %s" , seconds + "s", "special message", mt.toString() + " from " + dt.toString(), message));
		return true;
	}

	public boolean onMetadata(String key, String value)
	{
		System.out.println(String.format("%28s : %s = %s" , "metadata", key, value));
		return true;
	}

	public boolean onEndOfFile() {
		long maxTimestamp = 0;
		for(DataEntityCache c : s2.getCachedHandles()) {
			if ((c != null) && (c.lastAbsTimestamp.getValue() > maxTimestamp))
				maxTimestamp = c.lastAbsTimestamp.getValue();
		}
		
		double toSeconds = maxTimestamp / 1000000000.0;
		
    	String seconds = String.valueOf(Math.round(toSeconds * 100.0) / 100.0);	// Rounds to two decimal places
		System.out.println(String.format("%12s s %15s : %s" , seconds, "end-of-file", "/"));
		return true;
	}

	@Override
	public boolean onUnmarkedEndOfFile() {
		long maxTimestamp = 0;
		for(DataEntityCache c : s2.getCachedHandles()) {
			if ((c != null) && (c.lastAbsTimestamp.getValue() > maxTimestamp))
				maxTimestamp = c.lastAbsTimestamp.getValue();
		}

		double toSeconds = maxTimestamp / 1000000000.0;

		String seconds = String.valueOf(Math.round(toSeconds * 100.0) / 100.0);	// Rounds to two decimal places
		System.out.println(String.format("%12s s %15s : %s" , seconds, "", "no more data to be read"));
		return true;
	}

	public boolean onDefinition(byte handle, SensorDefinition definition)
	{
    	ValueType vt = ValueType.convert(definition.valueType);
    	AbsoluteId ai = AbsoluteId.convert(definition.absoluteId);

    	String vectorSize = "";
    	if (definition.vectorSize == 1)
    		vectorSize = "scalar";
    	else
    		vectorSize = String.valueOf(definition.vectorSize);

		System.out.println(String.format("%28s : %3s [%s] %s; name=%s, unit=%s, %s bits/sample + %s bits of padding, type=%s %s, %s, with additional %s bits of padding, "+
            "f_s=%s, y=%s*x-%s", "definition", handle, (char)handle, "sensor", definition.name, definition.unit, definition.resolution, definition.scalarBitPadding,
            vt.toString(), ai.toString(), vectorSize, definition.vectorBitPadding, definition.samplingFrequency, definition.k, definition.n ));
		return true;
	}

	public boolean onDefinition(byte handle, StructDefinition definition)
	{
		System.out.println(String.format("%28s : %3d [%s] %s; name=%s, elements=[%s]" , "definition", handle, handle, "struct", definition.name, definition.elementsInOrder ));
		return true;
	}

	public boolean onDefinition(byte handle, TimestampDefinition definition)
	{
		AbsoluteId ai = AbsoluteId.convert(definition.absoluteId);

		System.out.println(String.format("%28s : %3d [%s] %s; %s, size=%d B, resolution=%s s", "definition", handle, handle, "timestamp", ai.toString(), definition.byteSize, definition.multiplier));
		return true;
	}

	public boolean onTimestamp(long nanoSecondTimestamp) {
		double toSeconds = nanoSecondTimestamp * 1e-9;
    	String seconds = String.valueOf(Math.round(toSeconds * 100.0) / 100.0);	 // Rounds to two decimal places
		System.out.println(String.format("%12 s %15s : %s ns" , seconds + "s", "timestamp", nanoSecondTimestamp ));
		return true;
	}

	public boolean onStreamPacket(byte handle, long timestamp, int len, byte data[]) {
		String elements = s2.getEntityHandles(handle).elementsInOrder;
    	    	
    	ArrayList<Float> sensorData = new ArrayList<>();
    	MultiBitBuffer mbb = new MultiBitBuffer(data);	// Getting n-bit samples
        int getPosition = 0;
        for(int i = 0; i < elements.length(); i++) {
            int bitSize = s2.getEntityHandles((byte)elements.charAt(i)).sensorDefinition.resolution;
            int temp = mbb.getInt(getPosition, bitSize);
            sensorData.add(temp*s2.getEntityHandles((byte)elements.charAt(i)).sensorDefinition.k + s2.getEntityHandles((byte)elements.charAt(i)).sensorDefinition.n);
            getPosition += bitSize;
        }

    	double toSeconds = timestamp/1000000000.0;	// Converts nanoseconds into seconds
    	String seconds = String.valueOf(Math.round(toSeconds * 100.0) / 100.0);	 // Rounds to two decimal places

		System.out.println(String.format("%12s s %15s : sensor %s, time=%s, data=%s" , seconds, "stream packet", handle, timestamp, sensorData));
		return true;
	}

	public boolean onUnknownLineType(byte type, int len, byte data[])
	{
		long maxTimestamp = 0;
		for(DataEntityCache c : s2.getCachedHandles()) {
			if ((c != null) && (c.lastAbsTimestamp.getValue() > maxTimestamp))
				maxTimestamp = c.lastAbsTimestamp.getValue();
		}
		
		double toSeconds = maxTimestamp / 1000000000.0;
		
    	String seconds = String.valueOf(Math.round(toSeconds * 100.0) / 100.0);	 // Rounds to two decimal places

    	ArrayList<Float> sensorData = new ArrayList<>();
    	MultiBitBuffer mbb = new MultiBitBuffer(data);	// Getting 10-bit sample from 19-Bytes
        for (int i = 0; i < 14; ++i) {
            int temp = mbb.getInt(i*10, 10);
            sensorData.add(temp*1.0f);
        }
        int tenBitTime = mbb.getInt(14*10, 10); // 10-bit sample counter

		System.out.println(String.format("%12s %15s : type=%s [%s], sample counter=%s, samples=%s" , seconds + "s", "unknown", type, (char)type, tenBitTime, sensorData ));
		return true;
	}

	public boolean onError(int lineNum,  String error)
	{
		long maxTimestamp = 0;
		for (DataEntityCache c : s2.getCachedHandles()) {
			if ((c != null) && (c.lastAbsTimestamp.getValue() > maxTimestamp))
				maxTimestamp = c.lastAbsTimestamp.getValue();
		}
		
		double toSeconds = maxTimestamp / 1000000000.0;
		
		String seconds = String.valueOf(Math.round(toSeconds * 100.0) / 100.0);	 // Rounds to two decimal places
		System.out.println(String.format("%12s %15s : Line number=%s" , seconds + "s", "Error", lineNum ));
		return true;
	}
}
