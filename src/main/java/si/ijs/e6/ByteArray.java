package si.ijs.e6;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * Helper class for writing byte arrays
 */
public class ByteArray {
    static public int addSizedStringToArray(byte array[], int pos, String string) {
        int newPos = -1;
        if ((pos + string.length() < array.length) && (pos >= 0)) {
            array[pos] = (byte)string.length();
            System.arraycopy(string.getBytes(), 0, array, pos+1, string.length());
            newPos = pos + string.length() + 1;
        }
        return newPos;
    }

    static public int addFloatToArray(byte array[], int pos, float f) {
        return addArrayToArray(array, pos, ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putFloat(f).array());
    }

    static public int addIntToArray(byte array[], int pos, long num, int numBytes) {
        return addArrayToArray( array, pos, Arrays.copyOfRange(ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putLong(num).array(), 0,
                numBytes) );
    }

    static public int addDoubleToArray(byte array[], int pos, double f) {
        return addArrayToArray(array, pos, ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putDouble(f).array());
    }

    static public int addArrayToArray(byte array[], int pos, byte array2[]) {
        return addArrayToArray(array, pos, array2, array2.length);
    }

    static public int addArrayToArray(byte array[], int pos, byte array2[], int size) {
        int newPos = -1;
        if ((pos + size <= array.length) && (pos >= 0)) {
            System.arraycopy(array2, 0, array, pos, array2.length);
            newPos = pos + array2.length;
        }
        return newPos;
    }
}
