package si.ijs.e6;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class S2Test {

    /**
     * This test will mimic EcgDataStreamFile functionality
     */
    @org.junit.jupiter.api.Test
    void store() {
        String fname = "test_store.s2";
        // create an S2 object (file will also be created, but this should be delegated to after some data is ready to be written - TODO)
        S2 s2file = new S2();
        S2.StoreStatus s2storeStatus = s2file.store(new File(fname));
        assertTrue(s2storeStatus.isOk());

        s2storeStatus.setVersion(1, "PCARD");
        s2storeStatus.addMetadata("metadata", "dummy 1");
        assertTrue(s2storeStatus.isOk());
    }

    /**
     * This test will open a file and then open it again
     *
     * The created file should only contain one metadata field, from the
     * time it was reopened (only "metadata" = "dummy 2")
     */
    @org.junit.jupiter.api.Test
    void tryReopenFile() {
        String fname = "test_reopen.s2";
        S2 s2file;
        S2.StoreStatus s2storeStatus;

        // create an S2 object (file will also be created, but this should be delegated to after some data is ready to be written - TODO)
        s2file = new S2();
        s2storeStatus = s2file.store(new File(fname));
        assertTrue(s2storeStatus.isOk());

        s2storeStatus.setVersion(1, "PCARD");
        s2storeStatus.addMetadata("metadata", "dummy 1");
        assertTrue(s2storeStatus.isOk());

        s2file = new S2();
        s2storeStatus = s2file.store(new File(fname));
        assertTrue(s2storeStatus.isOk());

        s2storeStatus.setVersion(1, "PCARD");
        s2storeStatus.addMetadata("metadata", "dummy 2");
        assertTrue(s2storeStatus.isOk());
    }
}